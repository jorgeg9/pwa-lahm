var dataCacheName = 'shell-app-all';
var cacheName = 'shell-app';
var filesToCache = [
  '/',
  '/index.html',
  '/pedido.html',
  '/seguir.html',
  '/js/main.js',
  '/css/style.css',
  '/contactform/contactform.js',
  '/img/La-Huerta-Murciana/Agrytel-Naranja.png',
  '/img/La-Huerta-Murciana/limon-plano.png',
  '/img/La-Huerta-Murciana/limon-plano192.png',
  '/img/La-Huerta-Murciana/limon-plano96.png',
  '/img/La-Huerta-Murciana/limon-plano48.png',
  '/img/La-Huerta-Murciana/limon.jpeg'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Instalado');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Cacheando modelo shell app');
      return cache.addAll(filesToCache);
    })
  );
});
self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activado');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Borrada la cache antigua', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});
/*self.addEventListener('fetch', function(event) {
  console.log('[Service Worker] Fetch (Recuperado de cache)', event.request.url);
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        // IMPORTANT: Clone the request. A request is a stream and
        // can only be consumed once. Since we are consuming this
        // once by cache and once by the browser for fetch, we need
        // to clone the response.
        var fetchRequest = event.request.clone();

        return fetch(fetchRequest).then(
          function(response) {
            // Check if we received a valid response
            if(!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            var responseToCache = response.clone();

            caches.open(CACHE_NAME)
              .then(function(cache) {
                cache.put(event.request, responseToCache);
              });

            return response;
          }
        );
      })
    );
});*/
self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch (Recuperado de cache)', e.request.url);
  var dataUrl = 'https://query.yahooapis.com/v1/public/yql';
  if (e.request.url.indexOf(dataUrl) > -1) {
    /*
     * When the request URL contains dataUrl, the app is asking for fresh
     * weather data. In this case, the service worker always goes to the
     * network and then caches the response. This is called the "Cache then
     * network" strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
     */
    e.respondWith(
      caches.open(dataCacheName).then(function(cache) {
        return fetch(e.request).then(function(response){
          cache.put(e.request.url, response.clone());
          return response;
        });
      })
    );
  } else {
    /*
     * The app is asking for app shell files. In this scenario the app uses the
     * "Cache, falling back to the network" offline strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
     */
    e.respondWith(
      caches.match(e.request).then(function(response) {
        return response || fetch(e.request);
      })
    );
  }
});
